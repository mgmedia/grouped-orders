��          �       �      �     �     �     �  
   �     �     �            -   $  
   R     ]  
   f     q  )   }     �  %   �  B   �          4  E   C  �   �  .        @  �  X          '     8     O     [  	   w     �     �  -   �     �     �     �     �  ;        D  1   J  R   |     �     �  Z   �  �   Q  7   �     .	   Add to grouped orders Added Order at Date Back to list page Base Price Create final order Customer No. Grouped Orders Item Description MabaGroup Multimedia UG (haftungsbeschränkt) Order Date Order ID Order Part Order Price Order was created from grouped orders: %s Quantity There are no unprocessed order parts. This item can only be purchased by advent calendar ticket holders. Unprocessed Grouped Orders Variation Code Your cart didn't have any items that can be added to a grouped order. Your cart includes items that are only available to be purchased as grouped orders. Please use the button 'Add To Grouped Order' above. Your order part has been created successfully. https://maba-group.com/ Project-Id-Version: Grouped Orders
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-11-28 12:23+0000
PO-Revision-Date: 2019-11-28 12:24+0000
Last-Translator: mzwillus <m.zwillus@maba-group.com>
Language-Team: Deutsch
Language: de-DE
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.0; wp-5.3 Zum Adventkalender hinzufügen Auftrag erstellt Zurück zur Übersicht Preis (EUR) Adventkalender abschließen Kundennr. Adventkalender Artikelbeschreibung MabaGroup Multimedia UG (haftungsbeschränkt) Auftragsdatum Auftragsnummer Teilauftrag Auftragswert Der Adventkalender beinhaltet folgende Teilbestellungen: %s Menge Du hast noch nichts in den Adventkalender gelegt. Dieser Artikel kann nur mit einem gültigen Adventkalender-Ticket bestellt werden. Dein Adventkalender 2019 Artikelnummer Dein Warenkorb enthält keine Artikel, die dem Adventkalender hinzugefügt werden können. Dein Warenkorb enthält Artikel des Adventkalenders. Diesen Artikel musst Du Deiner Box hinzufügen. Verwende dazu den Button "Zum Adventkalender hinzufügen" oben. Deine Bestellung wurde dem Adventkalender hinzugefügt. https://maba-group.com/ 