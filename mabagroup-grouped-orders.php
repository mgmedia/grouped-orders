<?php
/**
 * Plugin Name: Grouped Orders
 * Author: MabaGroup Multimedia UG (haftungsbeschränkt)
 * Author URI: https://maba-group.com/
 * Text Domain: mabagroup-go
 * Version: 1.4.0
 */

/** @noinspection PhpUnused */

function mabagroup_go_activate_plugin()
{
    mabagroup_go_provision_users();
    mabagroup_go_flush_rewrite_rules();

    if ( ! wp_next_scheduled("mabagroup_go_cron_job")) {
        wp_schedule_event(time(), "hourly", "mabagroup_go_cron_job");
    }
}

register_activation_hook(__FILE__, "mabagroup_go_activate_plugin");

function mabagroup_go_initialize_plugin()
{
    load_plugin_textdomain("mabagroup-go", false, dirname(plugin_basename(__FILE__)).'/languages');
}

add_action("init", "mabagroup_go_initialize_plugin");

function mabagroup_go_add_acf_fields()
{
    if (function_exists('acf_add_local_field_group')):
        acf_add_local_field_group(
            array(
                'key'                   => 'group_5dd1e34d6b013',
                'title'                 => 'Grouped Orders',
                'fields'                => array(
                    array(
                        'key'               => 'field_5dd1e35af6491',
                        'label'             => 'Customer ID',
                        'name'              => 'customer_id',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 1,
                        'conditional_logic' => 0,
                        'wrapper'           => array(
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ),
                        'default_value'     => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ),
                    array(
                        'key'               => 'field_5dd1e384f6492',
                        'label'             => 'Order Date',
                        'name'              => 'order_date',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 1,
                        'conditional_logic' => 0,
                        'wrapper'           => array(
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ),
                        'default_value'     => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ),
                    array(
                        'key'               => 'field_5dd1e4e2aedef',
                        'label'             => 'Sales Lines',
                        'name'              => 'sales_lines',
                        'type'              => 'repeater',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => array(
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ),
                        'collapsed'         => '',
                        'min'               => 0,
                        'max'               => 0,
                        'layout'            => 'table',
                        'button_label'      => '',
                        'sub_fields'        => array(
                            array(
                                'key'               => 'field_5dd1e50aaedf0',
                                'label'             => 'Variation Code',
                                'name'              => 'variation_code',
                                'type'              => 'text',
                                'instructions'      => '',
                                'required'          => 1,
                                'conditional_logic' => 0,
                                'wrapper'           => array(
                                    'width' => '',
                                    'class' => '',
                                    'id'    => '',
                                ),
                                'default_value'     => '',
                                'placeholder'       => '',
                                'prepend'           => '',
                                'append'            => '',
                                'maxlength'         => '',
                            ),
                            array(
                                'key'               => 'field_5dd1e51daedf1',
                                'label'             => 'Quantity',
                                'name'              => 'quantity',
                                'type'              => 'number',
                                'instructions'      => '',
                                'required'          => 1,
                                'conditional_logic' => 0,
                                'wrapper'           => array(
                                    'width' => '',
                                    'class' => '',
                                    'id'    => '',
                                ),
                                'default_value'     => '',
                                'placeholder'       => '',
                                'prepend'           => '',
                                'append'            => '',
                                'min'               => '',
                                'max'               => '',
                                'step'              => '',
                            ),
                            array(
                                'key'               => 'field_5ddb3998c2a5c',
                                'label'             => 'Cart Item Key',
                                'name'              => 'cart_item_key',
                                'type'              => 'text',
                                'instructions'      => '',
                                'required'          => 0,
                                'conditional_logic' => 0,
                                'wrapper'           => array(
                                    'width' => '',
                                    'class' => '',
                                    'id'    => '',
                                ),
                                'default_value'     => '',
                                'placeholder'       => '',
                                'prepend'           => '',
                                'append'            => '',
                                'maxlength'         => '',
                            ),
                            array(
                                'key'               => 'field_5dd1e531aedf2',
                                'label'             => 'Base Price',
                                'name'              => 'base_price',
                                'type'              => 'text',
                                'instructions'      => '',
                                'required'          => 0,
                                'conditional_logic' => 0,
                                'wrapper'           => array(
                                    'width' => '',
                                    'class' => '',
                                    'id'    => '',
                                ),
                                'default_value'     => '',
                                'placeholder'       => '',
                                'prepend'           => '',
                                'append'            => '',
                                'maxlength'         => '',
                            ),
                            array(
                                'key'               => 'field_5dd1e566aedf3',
                                'label'             => 'Total Price',
                                'name'              => 'total_price',
                                'type'              => 'text',
                                'instructions'      => '',
                                'required'          => 0,
                                'conditional_logic' => 0,
                                'wrapper'           => array(
                                    'width' => '',
                                    'class' => '',
                                    'id'    => '',
                                ),
                                'default_value'     => '',
                                'placeholder'       => '',
                                'prepend'           => '',
                                'append'            => '',
                                'maxlength'         => '',
                            ),
                        ),
                    ),
                    array(
                        'key'               => 'field_5de64e3b2add1',
                        'label'             => 'Abgerechnet',
                        'name'              => 'abgerechnet',
                        'type'              => 'true_false',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => array(
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ),
                        'message'           => '',
                        'default_value'     => 0,
                        'ui'                => 0,
                        'ui_on_text'        => '',
                        'ui_off_text'       => '',
                    ),
                ),
                'location'              => array(
                    array(
                        array(
                            'param'    => 'post_type',
                            'operator' => '==',
                            'value'    => 'mabagroup_go',
                        ),
                    ),
                ),
                'menu_order'            => 0,
                'position'              => 'normal',
                'style'                 => 'default',
                'label_placement'       => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen'        => '',
                'active'                => true,
                'description'           => '',
            )
        );

        acf_add_local_field_group(
            array(
                'key'                   => 'group_5dd1da680d56a',
                'title'                 => 'Sammelbestellungen',
                'fields'                => array(
                    array(
                        'key'               => 'field_5dd1da75fb59e',
                        'label'             => 'Sammelbestellungen',
                        'name'              => 'sammelbestellungen',
                        'type'              => 'checkbox',
                        'instructions'      => 'Dieses Feld steuert, ob ein Produkt zu einer Sammelbestellungsart hinzugefügt werden kann.',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => array(
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ),
                        'choices'           => array(
                            'Allgemeine Sammelbestellungen' => 'Allgemeine Sammelbestellungen',
                            'Adventskalender'               => 'Adventskalender',
                        ),
                        'allow_custom'      => 0,
                        'default_value'     => array(),
                        'layout'            => 'vertical',
                        'toggle'            => 0,
                        'return_format'     => 'value',
                        'save_custom'       => 0,
                    ),
                ),
                'location'              => array(
                    array(
                        array(
                            'param'    => 'post_type',
                            'operator' => '==',
                            'value'    => 'product',
                        ),
                    ),
                ),
                'menu_order'            => 0,
                'position'              => 'side',
                'style'                 => 'default',
                'label_placement'       => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen'        => '',
                'active'                => true,
                'description'           => '',
            )
        );

        acf_add_local_field_group(
            array(
                'key'                   => 'group_5de1cde1333cd',
                'title'                 => 'Grouped Orders User Page',
                'fields'                => array(
                    array(
                        'key'               => 'field_5de1cdee0e8ed',
                        'label'             => 'Grouped Order Access',
                        'name'              => 'grouped_order_access',
                        'type'              => 'true_false',
                        'instructions'      => 'This field is updated by the grouped order plugin itself for the users who didn\'t have an account earlier. Do not change this field on your own.',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => array(
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ),
                        'message'           => '',
                        'default_value'     => 0,
                        'ui'                => 0,
                        'ui_on_text'        => '',
                        'ui_off_text'       => '',
                    ),
                ),
                'location'              => array(
                    array(
                        array(
                            'param'    => 'user_form',
                            'operator' => '==',
                            'value'    => 'edit',
                        ),
                        array(
                            'param'    => 'current_user_role',
                            'operator' => '==',
                            'value'    => 'administrator',
                        ),
                    ),
                ),
                'menu_order'            => 0,
                'position'              => 'normal',
                'style'                 => 'default',
                'label_placement'       => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen'        => '',
                'active'                => true,
                'description'           => '',
            )
        );
    endif;
}

add_action("acf/init", "mabagroup_go_add_acf_fields");

function mabagroup_go_register_post_types()
{
    $args = array(
        'label'               => __('Grouped Orders', 'mabagroup-go'),
        'supports'            => array('title', 'custom-fields'),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'show_in_admin_bar'   => false,
        'show_in_nav_menus'   => false,
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );
    register_post_type("mabagroup_go", $args);
}

add_action("init", "mabagroup_go_register_post_types");

add_action(
    "woocommerce_cart_collaterals",
    function () {
        if (mabagroup_go_has_customer_purchased_calendar_ticket()) :
            ?>
            <form action="<?php echo esc_url(admin_url('admin-post.php')) ?>" method="post">
                <input type="hidden" name="action" value="mggo_create">
                <button style="display: block;" type="submit"
                        class="button primary mt-0 pull-left small"><?php _e(
                        'Add to grouped orders',
                        'mabagroup-go'
                    ); ?></button>
            </form>
        <?php
        endif;
    }
);

add_action(
    "woocommerce_before_cart",
    function () {
        if (filter_input(INPUT_GET, "grouped-order") === "noItems") {
            mabagroup_go_load_css();
            printf(
                "<span id='mabagroup_go_message'>%s</span>",
                __("Your cart didn't have any items that can be added to a grouped order.", "mabagroup-go")
            );
        }
    }
);

function mabagroup_go_grouped_order_recently_created()
{
    $start_date = date("Y-m-d H:i", strtotime("+1 hour +2 minutes"));
    $end_date   = date("Y-m-d H:i", strtotime("+1 hour -2 minutes"));

    $user_id = get_current_user_id();

    $sql = "SELECT *
FROM `wp_pnrhsfyjvg_posts`
WHERE `post_type` = 'mabagroup_go' AND `post_date` > '{$end_date}' AND `post_date` < '{$start_date}' AND post_author = {$user_id}";

    global $wpdb;
    $r = $wpdb->get_results($sql);

    if (count($r)) {
        return true;
    }

    return false;
}

function mabagroup_go_add()
{
    if (mabagroup_go_grouped_order_recently_created()) {
        wp_die(
            __(
                "Sorry, there wasn't enough time between your previous grouped order and this one. Please try again later."
            )
        );
    }

    $user_id         = get_current_user_id();
    $session_handler = new WC_Session_Handler();

    $session    = $session_handler->get_session($user_id);
    $cart_items = maybe_unserialize($session['cart']);

    if (count($cart_items) > 0) {
        $groupable_elements = [];
        foreach ($cart_items as $cartItem) {
            $grouped_order_setting = get_field("sammelbestellungen", $cartItem["product_id"]);
            if (is_array($grouped_order_setting) && count($grouped_order_setting) > 0) {
                $groupable_elements[] = $cartItem;
            }
        }

        if (count($groupable_elements) === 0) {
            $redirect = wc_get_cart_url();
            $redirect = add_query_arg("grouped-order", "noItems", $redirect);
            wp_redirect($redirect);

            return;
        }

        $base_post = array(
            "post_title"  => get_user_by("ID", $user_id)->display_name,
            "post_type"   => "mabagroup_go",
            "post_status" => "draft",
            "post_author" => $user_id,
        );

        $mggo_order = wp_insert_post($base_post);

        if (is_int($mggo_order)) {
            update_field("customer_id", get_current_user_id(), $mggo_order);
            update_field(
                "order_date",
                date("Y-m-d H:i:s", strtotime(get_post($mggo_order)->post_date_gmt)),
                $mggo_order
            );
            foreach ($groupable_elements as $cartItem) {
                $variation_value = $cartItem["variation_id"];
                if ($variation_value === 0) {
                    $variation_value = $cartItem["product_id"];
                }

                $data = array(
                    "variation_code" => $variation_value,
                    "quantity"       => $cartItem["quantity"],
                    "base_price"     => wc_round_tax_total($cartItem["line_total"] / $cartItem["quantity"]),
                    "total_price"    => wc_round_tax_total($cartItem["line_total"]),
                    "cart_item_key"  => $cartItem["key"],
                );

                add_row("sales_lines", $data, $mggo_order);

                if ($cartItem["variation_id"] === 0) {
                    $product_id_for_stock_manipulation = $cartItem["product_id"];
                } else {
                    $product_id_for_stock_manipulation = $cartItem["variation_id"];
                }

                wc_update_product_stock($product_id_for_stock_manipulation, $cartItem["quantity"], "decrease");
            }

            $post              = get_post($mggo_order);
            $post->post_status = "publish";
            wp_update_post($post);
        }
    }

    $redirect = wc_get_account_endpoint_url("mabagroup-go");
    $redirect = add_query_arg("grouped-order", "orderPartAdded", $redirect);

    wp_redirect($redirect);
}

add_action("admin_post_mggo_create", "mabagroup_go_add");

function mabagroup_go_my_account_endpoints()
{
    add_rewrite_endpoint("mabagroup-go", EP_ROOT | EP_PAGES);
}

add_action("init", "mabagroup_go_my_account_endpoints");

/**
 * @param $vars array
 *
 * @return array
 */
function mabagroup_go_my_account_query_vars($vars)
{
    $vars[] = "mabagroup-go";

    return $vars;
}

add_filter("query_vars", "mabagroup_go_my_account_query_vars", 0);

/**
 * @param $items
 *
 * @return array
 */
function mabagroup_go_my_account_button($items)
{
    $items['mabagroup-go'] = __('Grouped Orders', "mabagroup-go");

    return $items;
}

add_filter('woocommerce_account_menu_items', 'mabagroup_go_my_account_button');

function mabagroup_go_my_account_go_list_content()
{
    $html_arg = filter_input(INPUT_GET, "grouped-order");

    if ("orderPartAdded" === $html_arg) {
        mabagroup_go_order_part_added_message();
        $html_arg = null;
    }

    if (null === $html_arg) {
        mabagroup_go_render_order_part_list();
    } else {
        mabagroup_go_render_order_part((int)$html_arg);
    }
}

function mabagroup_go_order_part_added_message()
{
    mabagroup_go_load_css();
    ?>
    <span id="mabagroup_go_message" style="background-color: green;">
        <?php _e("Your order part has been created successfully.", "mabagroup-go"); ?>
    </span>
    <?php

}

/**
 * @param int $order_id
 */
function mabagroup_go_render_order_part(int $order_id)
{
    $post_id = $order_id;
    $post    = get_post($post_id);
    ?>

    <dl>
        <dt><?php _e("Order Part", "mabagroup-go") ?></dt>
        <dd><?php echo $post->ID ?></dd>

        <dt><?php _e("Added Order at Date", "mabagroup-go") ?></dt>
        <dd><?php echo $post->post_date_gmt; ?></dd>
    </dl>

    <style>
        tfoot td {
            font-weight: bold;
        }
    </style>

    <table>
        <tr>
            <th><?php _e("Variation Code", "mabagroup-go"); ?></th>
            <th><?php _e("Item Description", "mabagroup-go") ?></th>
            <th><?php _e("Quantity", "mabagroup-go"); ?></th>
            <th><?php _e("Base Price", "mabagroup-go"); ?></th>
        </tr>
        <?php

        $sum = 0;
        foreach (
            get_field("sales_lines", $post_id) as $salesLine
        ) : $productDummy = wc_get_product($salesLine["variation_code"]); ?>

            <tr>
                <td><?php echo $salesLine["variation_code"]; ?></td>
                <td>
                    <?php

                    echo $productDummy->get_title();
                    if ($productDummy->get_attributes()) : ?>
                        <span class="mabagroup.go.item.attributes" style="display: block">
                            <?php _e("Item Attributes", "mabagroup-go"); ?>
                        </span>
                        <ul>
                            <?php foreach ($productDummy->get_attributes() as $attribute) : ?>
                                <li><?php echo $attribute; ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>

                </td>
                <td><?php echo $salesLine["quantity"]; ?></td>
                <td>
                    <?php echo wc_get_price_including_tax($productDummy); ?>
                </td>
            </tr>
            <?php $sum += wc_get_price_including_tax($productDummy) * $salesLine["quantity"]; endforeach; ?>
        <tfoot>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td><?php echo wc_format_decimal($sum); ?></td>
        </tr>
        </tfoot>
    </table>

    <a href="<?php echo get_site_url()."/mein-konto/mabagroup-go/"; ?>"><?php _e(
            "Back to list page",
            "mabagroup-go"
        ); ?></a>
    <?php

}

function mabagroup_go_render_order_part_list()
{
    global $wpdb;
    $current_user_id = get_current_user_id();
    $posts           = $wpdb->get_results(
        "SELECT ID FROM {$wpdb->posts} WHERE post_type = 'mabagroup_go' AND post_author = {$current_user_id} AND post_status = 'publish' ORDER BY post_date DESC;"
    );

    if ($first_element = get_post($posts[0]->ID)) {
        if (date("Y-m-d H:i", strtotime($first_element->post_date_gmt)) === date("Y-m-d H:i")) {
            $sales_lines = get_field("sales_lines", $first_element);
            foreach ($sales_lines as $salesLine) {
                WC()->cart->remove_cart_item($salesLine["cart_item_key"]);
            }
        }
    }

    ?>

    <h1><?php _e("Unprocessed Grouped Orders", "mabagroup-go"); ?></h1>
    <div class="woocommerce">
        <div class="woocommerce-MyAccount-content">
            <div class="woocommerce-notices-wrapper">
                <?php if (filter_input(INPUT_GET, "grouped-order") === "noOrderParts") : ?>
                    <span id="mabagroup_go_message"><?php _e(
                            "There are no unprocessed order parts.",
                            "mabagroup-go"
                        ); ?></span>
                <?php endif; ?>
            </div>

            <?php if (count($posts) > 0) : ?>
                <div class="touch-scroll-table">
                    <style>
                        .decimal {
                            text-align: right;
                        }
                    </style>
                    <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
                        <thead>
                        <tr>
                            <th class="woocommerce-orders-table__header">
                                    <span
                                            class="nobr"><?php _e("Order ID", "mabagroup-go"); ?></span></th>
                            <th class="woocommerce-orders-table__header"><span
                                        class="nobr"><?php _e("Customer No.", "mabagroup-go"); ?></span></th>
                            <th class="woocommerce-orders-table__header">
                                <span class="nobr"><?php _e("Order Date", "mabagroup-go"); ?></span></th>

                            <th class="woocommerce-orders-table__header">
                                <span class="nobr"><?php _e("Order Price", "mabagroup-go"); ?></span>
                            </th>

                            <th class="woocommerce-orders-table__header">
                                <span class="nobr"><?php _e("Status", "mabagroup-go"); ?></span>
                            </th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php

                        $sum = 0;

                        /**
                         * @var $postdata WP_Post
                         */
                        foreach ($posts as $postdata) : ?>
                            <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-on-hold order">
                                <td class="woocommerce-orders-table__cell">
                                    <?php /** @noinspection HtmlUnknownTarget */
                                    printf(
                                        "<a href='%s'>%s</a>",
                                        wc_get_account_endpoint_url("mabagroup-go")."?grouped-order={$postdata->ID}",
                                        $postdata->ID
                                    ); ?>
                                </td>
                                <td class="woocommerce-orders-table__cell">
                                    <?php echo get_field("customer_id", $postdata->ID); ?>
                                </td>
                                <td class="woocommerce-orders-table__cell">
                                    <?php echo get_post($postdata->ID)->post_date_gmt; ?>
                                </td>
                                <td class="woocommerce-orders-table__cell decimal">
                                    <?php

                                    $order_sales_lines = get_field("sales_lines", $postdata->ID);
                                    $order_base_price  = 0;
                                    foreach ($order_sales_lines as $order_sales_line) {
                                        $item_base_price  = wc_get_price_including_tax(
                                                                wc_get_product($order_sales_line["variation_code"])
                                                            ) * $order_sales_line["quantity"];
                                        $sum              += $item_base_price;
                                        $order_base_price += $item_base_price;
                                    }

                                    echo wc_format_decimal($order_base_price);
                                    ?>
                                </td>
                                <td class="woocommerce-orders-table__cell">
                                    <?php

                                    if (get_field("abgerechnet", $postdata->ID)) {
                                        _e("Abgerechnet", "mabagroup-go");
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="woocommerce-orders-table__cell decimal"><?php echo wc_format_decimal(
                                    $sum
                                ); ?></td>
                            <td></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>

                <?php if (date("Y-m-d", strtotime("+1 hours")) >= "2019-12-24") : ?>
                    <form action="<?php echo esc_url(admin_url('admin-post.php')); ?>" method="post">
                        <input type="hidden" name="action" value="mggo_merge">
                        <button type="submit" class="button primary mt-0 pull-left small">
                            <?php

                            _e(
                                'Create final order',
                                'mabagroup-go'
                            ); ?>
                        </button>

                        <?php printf(
                            "<p class='text-danger' style='color: red;'>%s</p>",
                            __(
                                "Achtung! Wenn beim Abschluss der Teilbestellungen Fehler auftreten, sende bitte eine E-Mail mit deiner Kundennummer oder einer Teilauftragsnummer an malte.zwillus@basement.consulting",
                                "mabagroup-go"
                            )
                        ); ?>
                    </form>
                <?php endif; ?>
            <?php else : ?>
                <span id="mabagroup_go_message"><?php _e(
                        "There are no unprocessed order parts.",
                        "mabagroup-go"
                    ); ?></span>
            <?php endif; ?>
        </div>
    </div>
    <?php

}

add_action('woocommerce_account_mabagroup-go_endpoint', 'mabagroup_go_my_account_go_list_content');

function mabagroup_go_merge()
{
    global $wpdb;

    $current_user_id = get_current_user_id();
    try {
        $wc_customer = new WC_Customer($current_user_id);
    } catch (Exception $e) {
        wp_die(__("Customer {$current_user_id} was not found!", "mabagroup-go"));
    }

    $query = "SELECT ID FROM {$wpdb->posts} WHERE post_type = 'mabagroup_go' AND post_author = {$current_user_id} AND post_status = 'publish' ORDER BY post_date DESC;";
    $posts = $wpdb->get_results($query);

    $j = 0;
    foreach ($posts as $post) {
        $post_id = $post->ID;

        if (get_field("abgerechnet", $post_id)) {
            $j++;
        }
    }

    if (count($posts) <= 0 || $j === count($posts)) {
        $redirect = wc_get_account_endpoint_url("mabagroup-go");
        $redirect = add_query_arg("grouped-order", "noOrderParts", $redirect);

        wp_redirect($redirect);
    }

    $order = wc_create_order(array('customer_id' => $current_user_id));

    $grouped_order_part_numbers = [];
    foreach ($posts as $go_line) {
        if (get_field("abgerechnet", $go_line->ID)) {
            continue;
        }
        $grouped_order_part_numbers[] = $go_line->ID;

        $sales_lines = get_field("sales_lines", $go_line->ID);
        foreach ($sales_lines as $salesLine) {
            $product = wc_get_product($salesLine["variation_code"]);
            try {
                $item_id = wc_add_order_item(
                    $order->get_id(),
                    array("order_item_name" => $product->get_title(), "order_item_type" => "line_item")
                );

                if ($item_id) {
                    wc_add_order_item_meta($item_id, '_qty', $salesLine['quantity']);
                    wc_add_order_item_meta($item_id, '_variation_id', $salesLine["variation_code"]);

                    wc_add_order_item_meta($item_id, '_tax_class', $product->get_tax_class());
                    wc_add_order_item_meta($item_id, '_product_id', $product->get_id());

                    wc_add_order_item_meta(
                        $item_id,
                        '_line_subtotal',
                        wc_get_price_excluding_tax($product) * $salesLine["quantity"]
                    );

                    wc_add_order_item_meta(
                        $item_id,
                        "_line_total",
                        wc_get_price_excluding_tax($product) * $salesLine["quantity"]
                    );

                    if (wc_get_product_variation_attributes($product->get_id()) && is_array(
                            wc_get_product_variation_attributes($product->get_id())
                        )) {
                        foreach (
                            wc_get_product_variation_attributes(
                                $product->get_id()
                            ) as $attribute_key => $attribute_value
                        ) {
                            wc_add_order_item_meta(
                                $item_id,
                                str_replace("attribute_", "", $attribute_key),
                                $attribute_value
                            );
                        }
                    }
                }
            } catch (Exception $e) {
                wp_die($e->getMessage());
            }
        }

        update_field("abgerechnet", true, $go_line->ID);
    }

    /** @noinspection PhpUndefinedVariableInspection */
    $order->set_address($wc_customer->get_billing(), 'billing');
    $order->set_address($wc_customer->get_shipping(), "shipping");

    $order->add_order_note(
        sprintf(
            __("Order was created from grouped orders: %s", "mabagroup-go"),
            implode(", ", $grouped_order_part_numbers)
        )
    );

    $order->save();
    $order->save_meta_data();

    $order->calculate_totals();

    $mailer = WC()->mailer();
    $mails  = $mailer->get_emails();

    foreach ($mails as $key => $mail) {
        /**
         * @var $mail WC_Email_New_Order
         */

        if ($mail->id == 'customer_processing_order') {
            $mail->trigger($order->get_id());
        }
    }

    wp_redirect($order->get_checkout_payment_url());
}

add_action("admin_post_mggo_merge", "mabagroup_go_merge");

/**
 * @return bool
 */
function mabagroup_go_has_customer_purchased_calendar_ticket()
{
    $bought_product = false;

    global $wpdb;
    $user_id = get_current_user_id();

    if ($user_id === 0) {
        return $bought_product;
    }

    if (get_field("grouped_order_access", "user_{$user_id}")) {
        return true;
    }

    $query   = "SELECT * FROM {$wpdb->postmeta} WHERE `meta_key` = '_customer_user' AND `meta_value` = '{$user_id}'";
    $results = $wpdb->get_results($query);

    foreach ($results as $result) {
        $wc_order = wc_get_order($result->post_id);
        if ($wc_order) {
            foreach ($wc_order->get_items() as $orderItem) {
                if ($orderItem["product_id"] === 94900) {
                    $bought_product = true;

                    return $bought_product;
                }
            }
        }
    }

    return $bought_product;
}

/**
 * @return bool
 */
function mabagroup_go_cart_has_groupable_items()
{
    /**
     * @var $woocommerce WooCommerce
     */
    global $woocommerce;

    $cart_items = $woocommerce->cart->get_cart();

    /**
     * @var $cart_item
     */
    foreach ($cart_items as $cart_item) {
        $product     = wc_get_product($cart_item["product_id"]);
        $field_value = get_field("sammelbestellungen", $product->get_id());

        if ((is_countable($field_value) && count($field_value) === 0) || null === $field_value || "" === $field_value) {
            continue;
        } else {
            return true;
        }
    }

    return false;
}

// add_action("init", "lets_get_the_inventory_debugged");

function lets_get_the_inventory_debugged()
{
    echo "<h1>Achtung, hier wird gerade gearbeitet.</h1>";

    $sql = "SELECT ID
FROM `wp_pnrhsfyjvg_posts`
WHERE `post_type` = 'shop_order' AND `post_date` > '2019-12-01 00:00:00' AND `post_date` < '2019-12-08 03:30:00'
ORDER BY `post_date`";

    global $wpdb;

    $allOrders = [];

    $r = $wpdb->get_results($sql);
    foreach ($r as $item) {
        $order_id    = $item->ID;
        $allOrders[] = $order_id;
    }

    $i         = 0;
    $inventory = array();

    foreach ($allOrders as $order_id) {
        $order = wc_get_order($order_id);
        /**
         * @var WC_Order_Item $orderItem
         */
        foreach ($order->get_items() as $orderItem) {
            $sammelbestellungen = get_field("sammelbestellungen", $orderItem->get_product_id());
            if (is_countable($sammelbestellungen) && count($sammelbestellungen) === 0) {
                $unique_code = $orderItem->get_variation_id();
                if ($unique_code === 0) {
                    $unique_code = $orderItem->get_product_id();
                }

                $factor = 1;
                if ($order->get_status() === "cancelled") {
                    $factor = -1;
                }

                $order_item_quantity                       = wc_get_order_item_meta($orderItem->get_id(), "_qty");
                $inventory[$unique_code][$order->get_id()] = $order_item_quantity * $factor;
                $i++;
            }
        }
    }


    var_dump($inventory);

    ?>
    <table style="border-style: solid;">
        <th>
            <tr>
                <td>Artikel-/variantennummer</td>
                <td>Eröffnugsbestand</td>
                <td>Korrekturwert</td>
                <td>Vorschlagswert</td>
            </tr>
        </th>

        <?php foreach ($inventory as $itemCode => $itemAssoc) {
            $mengeAusAllenBestellungen = 0;
            $alterWert                 = get_post_meta($itemCode, "_stock")[0];

            foreach ($itemAssoc as $quantity) {
                $mengeAusAllenBestellungen += $quantity;
            }
            $saldo = $alterWert - $mengeAusAllenBestellungen;
            ?>

            <tr>
                <td><?php echo $itemCode; ?></td>
                <td><?php echo $alterWert; ?></td>
                <td><?php echo $mengeAusAllenBestellungen; ?></td>
                <td><?php echo $saldo; ?></td>
            </tr>
        <?php } ?>
    </table>
    <?php echo $i;

    foreach ($inventory as $itemCode => $itemAssoc) {
        echo "<h2>$itemCode</h2>";
        echo "<table><tr><th>Auftragsnummer</th><th>Menge</th></tr>";
        $j = 0;
        foreach ($itemAssoc as $key => $value) : ?>
            <tr>
                <td><?php echo $key; ?></td>
                <td>
                    <?php $j += $value;
                    echo $value; ?>
                </td>
            </tr>

        <?php endforeach;
        echo "<tr><td></td><td><strong>$j</strong></td></tr>";
        echo "</table>";
    }

    wp_die();
}

if ( ! function_exists('woocommerce_button_proceed_to_checkout')) {
    /**
     *
     */
    function woocommerce_button_proceed_to_checkout()
    {
        if (mabagroup_go_cart_has_groupable_items()) {
            mabagroup_go_load_css();
            ?>
            <span id="mabagroup_go_message"><?php _e(
                    "Your cart includes items that are only available to be purchased as grouped orders. Please use the button 'Add To Grouped Order' above.",
                    "mabagroup-go"
                ); ?></span>
            <?php

            return;
        }

        wc_get_template('cart/proceed-to-checkout-button.php');
    }
}

function mabagroup_go_load_css()
{
    ?>
    <style>
        span#mabagroup_go_message {
            background-color: red;
            color: white;
            padding: 1em 0.5em;
            margin-bottom: 2em;
            display: block;
        }
    </style>
    <?php

}

if ( ! function_exists('woocommerce_template_single_add_to_cart')) {
    function woocommerce_template_single_add_to_cart()
    {
        $can_purchase = false;

        /**
         * @var $product WC_Product_Variable
         */
        global $product;
        $grouped_order_meta_field = get_field("sammelbestellungen", $product->get_id());

        if ((is_countable($grouped_order_meta_field) && count(
                                                            $grouped_order_meta_field
                                                        ) === 0) || null === $grouped_order_meta_field || "" === $grouped_order_meta_field || mabagroup_go_has_customer_purchased_calendar_ticket(
            )) {
            $can_purchase = true;
        }

        if ($can_purchase) {
            do_action('woocommerce_'.$product->get_type().'_add_to_cart');
        } else {
            mabagroup_go_load_css();
            ?>
            <span id="mabagroup_go_message"><?php _e(
                    "This item can only be purchased by advent calendar ticket holders.",
                    "mabagroup-go"
                ); ?></span>
            <?php

        }
    }
}

function mabagroup_go_check_mini_cart()
{
    if (mabagroup_go_cart_has_groupable_items()) {
        mabagroup_go_css_hide_buy_button();
    }
}

add_action("woocommerce_before_mini_cart", "mabagroup_go_check_mini_cart");

function mabagroup_go_css_hide_buy_button()
{
    ?>
    <style>
        a.button.checkout.wc-forward {
            display: none;
        }
    </style>
    <?php

}

function mabagroup_go_provision_users()
{
    global $wpdb;
    $debug           = false;
    $not_found_users = array();
    $i               = 0;

    $sql = 'SELECT * FROM wp_pnrhsfyjvg_woocommerce_order_itemmeta JOIN 
wp_pnrhsfyjvg_woocommerce_order_items ON 
wp_pnrhsfyjvg_woocommerce_order_itemmeta.order_item_id = wp_pnrhsfyjvg_woocommerce_order_items.order_item_id 
JOIN wp_pnrhsfyjvg_postmeta AS PostMeta1 ON wp_pnrhsfyjvg_woocommerce_order_items.order_id = PostMeta1.post_id 
JOIN wp_pnrhsfyjvg_postmeta AS PostMeta2 ON PostMeta1.post_id = PostMeta2.post_id 
WHERE wp_pnrhsfyjvg_woocommerce_order_itemmeta.meta_value = 94900 
AND PostMeta1.meta_key = "_customer_user" AND PostMeta1.meta_value <= 0 AND PostMeta2.meta_key = "_billing_email";';

    $results = $wpdb->get_results($sql);

    foreach ($results as $result) {
        $email_address = $result->meta_value;

        if ($user = get_user_by_email($email_address)) {
            update_field("grouped_order_access", true, "user_{$user->ID}");
            $i++;
        } else {
            $not_found_users[] = $email_address;
        }
    }

    if ($debug && count($not_found_users) > 0) {
        echo "<ul>";

        echo "<li>Results: ".count($results)."</li>";
        echo "<li>Processed: ".$i."</li>";

        foreach ($not_found_users as $notFoundUser) {
            echo "<li>{$notFoundUser}</li>";
        }

        echo "</ul>";
        wp_die();
    }
}

function mabagroup_go_flush_rewrite_rules()
{
    mabagroup_go_my_account_endpoints();
    mabagroup_go_register_post_types();
    flush_rewrite_rules();
}

add_action("mabagroup_go_cron_job", "mabagroup_go_flush_rewrite_rules");


function mabagroup_go_stop_stock_reduction($quantity, int $order_id)
{
    $order = wc_get_order($order_id);

    if ($order) {
        /**
         * @var WC_Order_Item $item
         */
        foreach ($order->get_items() as $item) {
            if ($item->get_variation_id() !== 0) {
                $post_id = $item->get_variation_id();
            } else {
                $post_id = $item->get_product_id();
            }

            $grouped_order = get_field("sammelbestellungen", $post_id);

            if ($grouped_order === "" || $grouped_order === null || (is_countable($grouped_order) && count(
                                                                                                         $grouped_order
                                                                                                     ) === 0)) {
                continue;
            } else {
                return false;
            }
        }
    }

    return true;
}

add_action("woocommerce_payment_complete_reduce_order_stock", "mabagroup_go_stop_stock_reduction", 10, 2);

function mabagroup_go_custom_column_definition($columns)
{
    $columns['customer'] = __('Customer', 'mabagroup_go');

    return $columns;
}

add_filter('manage_mabagroup_go_posts_columns', 'mabagroup_go_custom_column_definition');

function mabagroup_go_custom_column_action($column, $post_id)
{
    if ($column === "customer") {
        $user_id = get_field("customer_id", $post_id);
        /**
         * @var WP_User $user
         */
        $user = get_user_by("ID", $user_id);

        echo $user->display_name;
    }
}

add_action('manage_mabagroup_go_posts_custom_column', 'mabagroup_go_custom_column_action', 10, 2);
